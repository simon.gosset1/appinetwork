# Exemples for APPINetwork

This gitlab contains the exemples for [Appinetwork](https://forgemia.inra.fr/GNet/appinetwork), a network construction and analysis software.

## File description :

**BIOGRID-ORGANISM-Saccharomyces_cerevisiae_S288c-4.2.191.tab2.txt** and **saccharomy_intact.txt** are the Biogrid and intact unformatted database for _Saccharomyces cerevisiae_

**input_ELP123456.txt** is the file containing the list of proteins given as an input to construct the network.

**uniprot-proteome_UP000002311.txt** is the uniprot proteome file use to create the thesaurus file (file that is used to map all protein identifiers).
